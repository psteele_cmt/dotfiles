# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/Users/psteele/.oh-my-zsh"

ZSH_THEME="ys"

# Standard plugins can be found in ~/.oh-my-zsh/plugins/
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(git zsh-z)

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='emacs'
else
  export EDITOR='emacs'
fi

# pyenv setup
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
export WORKON_HOME=~/.virtualenvs
pyenv virtualenvwrapper_lazy

export CMT_HOME=~/cmt
export LANG=en_US.UTF-8

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh" || true

# Turn off auto-title, so we can manually set titles
export DISABLE_AUTO_TITLE=true

export PYTHONPATH=/Users/psteele/cmt/vtrack:/Users/psteele/cmt/vtrack/mm:/Users/psteele/cmt/vtrack/lib:/Users/psteele/cmt:/Users/psteele/cmt/vtrack/appserver:/Users/psteele/cmt/vtrack/base

# export PATH="$HOME/.poetry/bin:$PATH"

export PATH=/Users/psteele/.local/bin:$PATH

autoload bashcompinit
bashcompinit
eval "$(register-python-argcomplete rc-helper)"

source ~/.secrets
